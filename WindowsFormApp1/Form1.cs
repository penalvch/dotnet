﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Runtime;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        connectiondb con1 = new connectiondb();
        connectiondb con2 = new connectiondb();
        private static ArrayList Listdate = new ArrayList();
        private static ArrayList Listsource = new ArrayList();
        private static ArrayList Listtitle = new ArrayList();
        private static ArrayList Listlink = new ArrayList();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'usstocksDataSet.form' table. You can move, or remove it, as needed.
            //this.formTableAdapter.Fill(this.usstocksDataSet.form);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            process();
        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void process()
        {
            dataGridView1.DataSource = null;
            Listdate.Clear();
            Listsource.Clear();
            Listtitle.Clear();
            Listlink.Clear();
            DataTable table = new DataTable();
            table.Columns.Add("date", typeof(string));
            table.Columns.Add("source", typeof(string));
            table.Columns.Add("title", typeof(string));
            table.Columns.Add("link", typeof(string));
            dataGridView1.DataSource = table;
            if (textBox1.Text != "")
            {
                try
                {
                    string symbol = textBox1.Text;
                    con1.Open();
                    string symbolsquery = "select cik,coname,conamep,gnrss,rss,rssevents,rsssec,sedar from symbols where symbol='" + symbol + "'";
                    MySqlDataReader symbolsrow;
                    symbolsrow = con1.ExecuteReader(symbolsquery);
                    if (symbolsrow.HasRows)
                    {
                        while (symbolsrow.Read())
                        {
                            string debugtext = "";
                            //BENZINGA TODO
                            //CIK
                            if (!DBNull.Value.Equals(symbolsrow["cik"]))
                            {
                                char[] sp = { ':' };
                                string[] cik_f = symbolsrow["cik"].ToString().Split(sp, StringSplitOptions.RemoveEmptyEntries);
                                debugtext += "cik_f=" + System.Environment.NewLine;
                                foreach (string cik in cik_f)
                                {
                                    debugtext += cik + System.Environment.NewLine;
                                }
                                string formquery = "select date,source,title,link from form where cik like '%" + cik_f[0] + "%'";
                                con2.Open();
                                // FORM
                                MySqlDataReader formrows;
                                formrows = con2.ExecuteReader(formquery);
                                if (formrows.HasRows)
                                {
                                    while (formrows.Read())
                                    {
                                        string dateraw = formrows["date"].ToString();
                                        CultureInfo ci;
                                        ci = new CultureInfo("en-US");
                                        string datestring = null;
                                        if (dateraw.EndsWith(" EST", true, ci))
                                        {
                                            datestring = dateraw.Substring(0, dateraw.Length - 4);
                                        }
                                        else
                                        {
                                            datestring = dateraw;
                                        }
                                        DateTime datetimeraw = Convert.ToDateTime(datestring);
                                        string date = datetimeraw.ToString("yyyy-MM-dd H:mm:ss");
                                        Listdate.Add(date);
                                        Listsource.Add(formrows["source"].ToString());
                                        Listtitle.Add(formrows["title"].ToString());
                                        Listlink.Add(formrows["link"].ToString());
                                    }
                                }
                                con2.Close();
                            }
                            // CONAME
                            if (!DBNull.Value.Equals(symbolsrow["coname"]))
                            {
                                string coname_f = symbolsrow["coname"].ToString();
                                debugtext += "coname_f=" + coname_f + System.Environment.NewLine;
                                textBox3.Text = coname_f;
                            }
                            // CONAMEP
                            if (!DBNull.Value.Equals(symbolsrow["conamep"]))
                            {
                                char[] sp = { '|' };
                                string[] conamep_f = symbolsrow["conamep"].ToString().Split(sp, StringSplitOptions.RemoveEmptyEntries);
                                debugtext += "conamep_f=" + System.Environment.NewLine;
                                foreach (string conamep in conamep_f)
                                {
                                    debugtext += conamep + System.Environment.NewLine;
                                }
                            }
                            // GNRSS
                            if (!DBNull.Value.Equals(symbolsrow["gnrss"]))
                            {
                                // Check if form contains gnrss, if not async get it
                                /*
                                Process gnrss_p = new Process();
                                gnrss_p.StartInfo.FileName = "pwsh.exe";
                                gnrss_p.StartInfo.Arguments = "-command {-symbol " + symbol + "}";
                                */
                                //DEBUG
                                List<string> gnrss_f = new List<string>();
                                gnrss_f.Add(symbolsrow["gnrss"].ToString());
                                debugtext += "gnrss_f=" + symbolsrow["gnrss"].ToString() + System.Environment.NewLine;
                            }
                            // RSS
                            if (!DBNull.Value.Equals(symbolsrow["rss"]))
                            {
                                List<string> rss_f = new List<string>();
                                rss_f.Add(symbolsrow["rss"].ToString());
                                debugtext += "rss_f=" + symbolsrow["rss"].ToString() + System.Environment.NewLine;
                            }
                            // RSS EVENTS
                            if (!DBNull.Value.Equals(symbolsrow["rssevents"]))
                            {
                                List<string> rssevents_f = new List<string>();
                                rssevents_f.Add(symbolsrow["rssevents"].ToString());
                                debugtext += "rssevents_f=" + symbolsrow["rssevents"].ToString() + System.Environment.NewLine;
                            }
                            // RSS SEC
                            if (!DBNull.Value.Equals(symbolsrow["rsssec"]))
                            {
                                List<string> rsssec_f = new List<string>();
                                rsssec_f.Add(symbolsrow["rsssec"].ToString());
                                debugtext += "rsssec_f=" + symbolsrow["rsssec"].ToString() + System.Environment.NewLine;
                            }
                            // SEDAR
                            if (!DBNull.Value.Equals(symbolsrow["sedar"]))
                            {
                                string sedar_f = symbolsrow["sedar"].ToString();
                                debugtext += "sedar_f=" + symbolsrow["sedar"].ToString() + System.Environment.NewLine;
                            }
                            textBox2.Text = debugtext;
                        }
                    }

                    con1.Close();
                }
                catch (Exception err)
                {
                    MessageBox.Show(err.ToString());
                }
                if (Listdate.Count > 0)
                {
                    for (int i = 0; i < Listdate.Count; i++)
                    {
                        // Supposedly change this to datatable being binded to gridview
                        table.Rows.Add(Listdate[i], Listsource[i], Listtitle[i], Listlink[i]);
                        table.DefaultView.Sort = "date desc";
                        dataGridView1.DataSource = table;
                    }
                }
            }
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if((int)e.KeyCode == (int)Keys.Enter)
            {
                process();
            }
        }
     }
}